export const enableSelection = target => {
    if (typeof target.onselectstart != "undefined") {
        target.onselectstart = function () {
            return true
        }
    } else if (typeof target.style.MozUserSelect != "undefined") {
        target.style.MozUserSelect = "auto";
    } else {
        
        target.onmousedown = function () {
            return true
        };
    }
    
    target.style.cursor = "auto"
};

export const enableSelection__woocrack = () => {
    let selection_interval_index = 0;
    let selection_interval = setInterval(() => {
        if (selection_interval_index === 5) {
            clearInterval(selection_interval)
        }
        
        enableSelection(document.body)
    }, 1000);
    
};

export const handleSalesPageClick = () => {
    const sales_page = document.querySelectorAll('.sales_page ');
    sales_page.forEach(sale_page => {
        sale_page.addEventListener('click', function () {
            window.open(this.innerText.match(/(https?:\/\/[^\s]+)/g), '_blank')
        });
    })
};

export const copyToClipboard = str => {
    debugger;
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
};

export const getPageType = (pageTypes = []) => {

}

