@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.woocrack-plugins-downloads.title')</h3>

    {!! Form::model($woocrack_plugins_download, ['method' => 'PUT', 'route' => ['admin.woocrack_plugins_downloads.update', $woocrack_plugins_download->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('download_url', trans('quickadmin.woocrack-plugins-downloads.fields.download-url').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('download_url', old('download_url'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('download_url'))
                        <p class="help-block">
                            {{ $errors->first('download_url') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('version', trans('quickadmin.woocrack-plugins-downloads.fields.version').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('version', old('version'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('version'))
                        <p class="help-block">
                            {{ $errors->first('version') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('last_update', trans('quickadmin.woocrack-plugins-downloads.fields.last-update').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('last_update', old('last_update'), ['class' => 'form-control datetime', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('last_update'))
                        <p class="help-block">
                            {{ $errors->first('last_update') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('downloaded', 'Downloaded', ['class' => 'control-label']) !!}
                    {!! Form::text('downloaded', old('downloaded'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('downloaded'))
                        <p class="help-block">
                            {{ $errors->first('downloaded') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('plugin_url_id', trans('quickadmin.woocrack-plugins-downloads.fields.plugin-url').'', ['class' => 'control-label']) !!}
                    {!! Form::select('plugin_url_id', $plugin_urls, old('plugin_url_id'), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('plugin_url_id'))
                        <p class="help-block">
                            {{ $errors->first('plugin_url_id') }}
                        </p>
                    @endif
                </div>
            </div>

        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });

            $('.datetime').datetimepicker({
                format: "{{ config('app.datetime_format_moment') }}",
                locale: "{{ App::getLocale() }}",
                sideBySide: true,
            });

        });
    </script>

@stop
