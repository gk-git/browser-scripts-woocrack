<?php

Route::group( [ 'prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.' ], function () {
    Route::apiResource( 'woocrack_plugins', 'WoocrackPluginController' );
    Route::post( 'woocrack_plugins_add_sets', 'WoocrackPluginController@stores' );

    Route::apiResource( 'woocrack_plugins_downloads', 'WoocrackPluginsDownloadController' );
    Route::post( 'woocrack_plugins_downloads/add_sets', 'WoocrackPluginsDownloadController@stores' );

    Route::get( 'download_plugins/{start}/{end}', 'WoocrackPluginsDownloadController@downloads' );
    Route::get( 'download_plugin/{plugin_id}', 'WoocrackPluginsDownloadController@download_plugin' );
    Route::post( 'save_downloads_url', 'WoocrackPluginsDownloadController@save_downloads_url' );
    Route::post( 'plugin_exist', 'WoocrackPluginsDownloadController@plugin_exist' );

} );
