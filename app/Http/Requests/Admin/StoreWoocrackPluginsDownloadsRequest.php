<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreWoocrackPluginsDownloadsRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'download_url'  => 'required|unique:woocrack_plugins_downloads,download_url,' . $this->route( 'woocrack_plugins_download' ),
            'version'       => 'required',
            'last_update'   => 'required|date_format:' . config( 'app.date_format' ) . ' H:i:s',
            'plugin_url_id' => 'required|exists:woocrack_plugins,id',
        ];
    }
}
