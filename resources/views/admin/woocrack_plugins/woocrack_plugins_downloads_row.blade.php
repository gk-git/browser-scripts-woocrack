<tr data-index="{{ $index }}">
    <td>{!! Form::text('woocrack_plugins_downloads['.$index.'][download_url]', old('woocrack_plugins_downloads['.$index.'][download_url]', isset($field) ? $field->download_url: ''), ['class' => 'form-control']) !!}</td>
<td>{!! Form::text('woocrack_plugins_downloads['.$index.'][version]', old('woocrack_plugins_downloads['.$index.'][version]', isset($field) ? $field->version: ''), ['class' => 'form-control']) !!}</td>

    <td>
        <a href="#" class="remove btn btn-xs btn-danger">@lang('quickadmin.qa_delete')</a>
    </td>
</tr>