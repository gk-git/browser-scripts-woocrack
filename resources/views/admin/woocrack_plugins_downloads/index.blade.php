@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.woocrack-plugins-downloads.title')</h3>
    @can('woocrack_plugins_download_create')
        <p>
            <a href="{{ route('admin.woocrack_plugins_downloads.create') }}"
               class="btn btn-success">@lang('quickadmin.qa_add_new')</a>

        </p>
    @endcan

    @can('woocrack_plugins_download_delete')
        <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.woocrack_plugins_downloads.index') }}"
                   style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a>
            </li>
            |
            <li><a href="{{ route('admin.woocrack_plugins_downloads.index') }}?show_deleted=1"
                   style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a>
            </li>
        </ul>
        </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table
                class="table table-bordered table-striped ajaxTable @can('woocrack_plugins_download_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                <tr>
                    @can('woocrack_plugins_download_delete')
                        @if ( request('show_deleted') != 1 )
                            <th style="text-align:center;"><input type="checkbox" id="select-all"/></th>@endif
                    @endcan

                    <th>@lang('quickadmin.woocrack-plugins-downloads.fields.download-url')</th>
                    <th>@lang('quickadmin.woocrack-plugins-downloads.fields.version')</th>
                    <th>@lang('quickadmin.woocrack-plugins-downloads.fields.downloaded')</th>
                    <th>@lang('quickadmin.woocrack-plugins-downloads.fields.last-update')</th>
                    <th>@lang('quickadmin.woocrack-plugins-downloads.fields.plugin-url')</th>
                    @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                    @else
                        <th>&nbsp;</th>
                    @endif
                </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        @can('woocrack_plugins_download_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.woocrack_plugins_downloads.mass_destroy') }}';
        @endif
        @endcan
        $(document).ready(function() {
            window.dtDefaultOptions.ajax = '{!! route('admin.woocrack_plugins_downloads.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [
                    @can('woocrack_plugins_download_delete')
                    @if ( request('show_deleted') != 1 )
                {
                    data: 'massDelete', name: 'id', searchable: false, sortable: false,
                },
                    @endif
                    @endcan
                {
                    data: 'download_url', name: 'download_url',
                },
                {data: 'version', name: 'version'},
                {data: 'downloaded', name: 'downloaded'},
                {data: 'last_update', name: 'last_update'},
                {data: 'plugin_url.url', name: 'plugin_url.url'},

                {data: 'actions', name: 'actions', searchable: false, sortable: false},
            ];
            processAjaxTables();

            $('.panel').on('click', '.download-button', function() {
                $.get('https://browserscripts.tk/api/v1/download_plugin/' + $(this).data('id'),
                    function(data, status) {
                        console.log('done' + $(this).data('id'));
                    });

            });
        });
    </script>
@endsection
