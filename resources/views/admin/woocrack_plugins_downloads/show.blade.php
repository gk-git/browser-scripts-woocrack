@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.woocrack-plugins-downloads.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.woocrack-plugins-downloads.fields.download-url')</th>
                            <td field-key='download_url'>{{ $woocrack_plugins_download->download_url }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.woocrack-plugins-downloads.fields.version')</th>
                            <td field-key='version'>{{ $woocrack_plugins_download->version }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.woocrack-plugins-downloads.fields.last-update')</th>
                            <td field-key='last_update'>{{ $woocrack_plugins_download->last_update }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.woocrack_plugins_downloads.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.datetime').datetimepicker({
                format: "{{ config('app.datetime_format_moment') }}",
                locale: "{{ App::getLocale() }}",
                sideBySide: true,
            });
            
        });
    </script>
            
@stop
