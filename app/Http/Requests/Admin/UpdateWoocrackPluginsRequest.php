<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateWoocrackPluginsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'url' => 'required|unique:woocrack_plugins,url,'.$this->route('woocrack_plugin'),
            'woocrack_plugins_downloads.*.download_url' => 'required|unique:woocrack_plugins_downloads,download_url,'.$this->route('woocrack_plugins_download'),
            'woocrack_plugins_downloads.*.version' => 'required',
        ];
    }
}
