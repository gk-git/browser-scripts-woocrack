<?php
/**
 * Created by PhpStorm.
 * User: gabykaram
 * Date: 12/9/18
 * Time: 3:33 AM
 */

/**
 * Function : Download a remote file at a given URL and save it to a local folder.
 * Input :
 * $url - URL of the remote file
 * $toDir - Directory where the remote file has to be saved once downloaded.
 * $withName - The name of file to be saved as.
 * Output :
 * true - if success
 * false - if failed
 *
 * Note : This function does not work in the Codelet due to network restrictions
 * but does work when executed from command line or from within a webserver.
 */
function downloadFile( $url, $toDir, $withName ) {

	$curlCh = curl_init();
	curl_setopt( $curlCh, CURLOPT_URL, $url );
	curl_setopt( $curlCh, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $curlCh, CURLOPT_SSLVERSION, 3 );
	$curlData = curl_exec( $curlCh );
	curl_close( $curlCh );


	$downloadPath = 'downloads/' . $withName;
	$file         = fopen( $downloadPath, "w+" );
	fputs( $file, $curlData );
	fclose( $file );
} // end
