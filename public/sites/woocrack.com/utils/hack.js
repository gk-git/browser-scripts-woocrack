export const getPluginsUrl = () => {
    let urls_array = [];
    document.querySelectorAll('ul.index li a').forEach(
        a_element => {
            const plugin_url = a_element.getAttribute('href').replace(/\/?$/, '/')
            urls_array.push({URL: plugin_url});
        }
    );
    
    return urls_array;
};

export const save_plugin_download_url = () => {
    const plugin_downloadURL_onclick = document.getElementById('download');
    if (plugin_downloadURL_onclick === null) {
        return;
    }
    let plugin_downloadURL = plugin_downloadURL_onclick.attributes['download'].replace(
        'location.href=\'', '').replace('\'', '');
    
    let plugin_url = window.location.href;
    const plugin_author_url = document.querySelector('.sales_page').innerText.match(/(https?:\/\/[^\s]+)/g)[0];
    const plugin_last_update = document.querySelector('.subscription_benefits li:last-child b').innerText;
    const plugin_last_update_date_msec = Date.parse(plugin_last_update);
    const plugin_last_update_date = new Date(plugin_last_update_date_msec);
    
    const plugin_last_update_day = plugin_last_update_date.getDate();
    const plugin_last_update_month = plugin_last_update_date.getMonth();
    const plugin_last_update_year = plugin_last_update_date.getFullYear();
    
    let plugin_version = getPluginVersion();
    
    save_plugin_download_data({
        plugin_url: plugin_url,
        plugin_download_url: plugin_downloadURL,
        plugin_last_update_day: plugin_last_update_day,
        plugin_last_update_month: plugin_last_update_month,
        plugin_last_update_year: plugin_last_update_year,
        plugin_version: plugin_version,
        plugin_author_url: plugin_author_url,
    });
};

export const save_plugin_download_data = (props) => {
    const {
        plugin_url,
        plugin_download_url,
        plugin_last_update_day,
        plugin_last_update_month,
        plugin_last_update_year,
        plugin_version,
        plugin_author_url
    } = props;
    fetch('https://browserscripts.tk/api/v1/save_downloads_url',
        {
            body: {
                plugin_url,
                plugin_download_url,
                plugin_last_update_day,
                plugin_last_update_month,
                plugin_last_update_year,
                plugin_version,
                plugin_author_url,
            }
        }).then(() => {
        if (window.location.href.indexOf('close_browser') > 0) {
            window.close();
        }
    })
};

export const getPluginVersion = () => {
    
    let return_version = '';
    document.querySelectorAll('.subscription_benefits li').forEach(subscribtion_benefits => {
        let version = subscribtion_benefits.innerHTML;
        if (version.indexOf('Product Version : ') !== -1) {
            version = version.replace(
                'Product Version : <span class="label label-success">', '');
            return_version = version.replace('</span><b></b>', '');
        }
    });
    
    return return_version;
};

const sleep = (milliseconds) => {
    var start = new Date().getTime();
    for (let i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
};

export const hackWoocrack = () => {
    if (window.location.href.indexOf('woocrack.com/catalog') > 0) {
        localStorage.setItem('plugins_urls', JSON.stringify(getPluginsUrl()));
    } else if (window.location.href.indexOf('woocrack.com/download/') > 0) {
        save_plugin_download_url();
    }
};
