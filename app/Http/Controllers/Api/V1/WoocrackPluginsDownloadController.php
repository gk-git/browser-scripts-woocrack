<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreWoocrackPluginsDownloadsRequest;
use App\Http\Resources\WoocrackPluginsDownloadResource;
use App\WoocrackPlugin;
use App\WoocrackPluginsDownload;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Storage;

class WoocrackPluginsDownloadController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return WoocrackPluginsDownloadResource::collection( WoocrackPluginsDownload::all() );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( StoreWoocrackPluginsDownloadsRequest $request ) {
        $woocrack_plugin = WoocrackPluginsDownload::create( $request->toArray() );

        return new WoocrackPluginsDownloadResource( $woocrack_plugin );
    }

    /**
     * Stores a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function stores( StoreWoocrackPluginsDownloadsRequest $request ) {
        $woocrack_plugin = WoocrackPluginsDownload::create( $request->toArray() );

        return new WoocrackPluginsDownloadResource( $woocrack_plugin );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show( WoocrackPluginsDownload $id ) {
        //
        return new WoocrackPluginsDownloadResource( $id );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, WoocrackPluginsDownload $id ) {
        //
        $id->update( $request->only( [ 'title', 'description' ] ) );

        return new WoocrackPluginsDownloadResource( $id );
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( WoocrackPluginsDownload $id ) {
        //
        $id->delete();

        return response()->json( null, 204 );
    }


    public function download_plugin_to_storage( $url, $version ): bool {
        $contents      = file_get_contents( $url );
        $name_from_url = substr( $url, strrpos( $url, '/' ) + 1 );
        $name          = str_replace( '.zip', $version . '.zip', $name_from_url );
        try {

            $storage = Storage::put( 'woocrack/' . $name, $contents );

            return true;
        } catch ( \Exception $e ) {
            return false;
        }
    }

    public function downloads( $start = 0, $end = 0 ) {
        if ( (int) $start >= 0 & $end > $start ) {
            $plugins = WoocrackPluginsDownload::where( 'downloaded', '=', 'false' )->get()->all();


            foreach ( $plugins as $key => $plugin ) {
                if ( $key < $start || $key > $end ) {
                    continue;
                }
                if ( $plugin->downloaded === 'false' ) {
                    $downloaded = $this->download_plugin_to_storage( $plugin->download_url, $plugin->version );
                    if ( $downloaded ) {
                        $plugin->update( [ 'downloaded' => 'true' ] );
                    }
                }

            }
            $plugins = WoocrackPluginsDownload::where( 'downloaded', '=', 'false' )->get()->toArray();

            return \count($plugins);
        }

        return response()->json( array( 'error' => 'not deleted' ), 204 );

    }

    public function download_plugin( $plugin_id ) {
        $plugin = WoocrackPluginsDownload::whereId( $plugin_id )->where( 'downloaded', '=', 'false' )->first();

        $downloaded = $this->download_plugin_to_storage( $plugin->download_url, $plugin->version );
        if ( $downloaded ) {
            $plugin->update( [ 'downloaded' => 'true' ] );
        }

    }

    public function save_downloads_url( Request $request ) {
        $plugin_url               = strtok( $request->input( 'plugin_url' ), '?' );
        $plugin_download_url      = $request->input( 'plugin_download_url' );
        $plugin_last_update_day   = $request->input( 'plugin_last_update_day' );
        $plugin_last_update_month = $request->input( 'plugin_last_update_month' );
        $plugin_last_update_year  = $request->input( 'plugin_last_update_year' );
        $plugin_version           = $request->input( 'plugin_version' );
        $plugin_author_url        = $request->input( 'plugin_author_url' );

        $plugin_last_update = Carbon::parse( $this->getFullMonth( $plugin_last_update_month ) . ' ' . $plugin_last_update_day . ', ' . $plugin_last_update_year )->format( 'Y-m-d H:i:s' );
//        $plugin_last_update = new \DateTime( $this->getFullMonth( $plugin_last_update_month ) . ' ' . $plugin_last_update_day . ', ' . $plugin_last_update_year );
        $plugin = WoocrackPlugin::whereUrl( $plugin_url )->first();
        $plugin->update(
            array(
                'author_url' => $plugin_author_url,
            )
        );
        $plugin_download = WoocrackPluginsDownload::whereDownloadUrl( $plugin_download_url )->get()->toArray();
        if ( \count( $plugin_download ) === 0 ) {
            $woocrack_plugin = WoocrackPluginsDownload::create(
                [
                    'downloaded'    => 'false',
                    'download_url'  => $plugin_download_url,
                    'version'       => $plugin_version,
                    'last_update'   => $plugin_last_update,
                    'plugin_url_id' => $plugin->id,
                ]
            );

            return new WoocrackPluginsDownloadResource( $woocrack_plugin );
        }

        return $plugin_download[0];

    }

//
    public function plugin_exist( Request $request ) {
        $plugin_url = $request->input( 'plugin_url' );

        $plugin = WoocrackPlugin::whereUrl( $plugin_url )->first();
        if ( $plugin !== null ) {

            $plugin_downloads = WoocrackPluginsDownload::wherePluginUrlId( $plugin->id )->get()->toArray();

            return response()->json( [
                'success'               => true,
                'plugin_exist'          => true,
                'plugin_download_exist' => \count( $plugin_downloads ) > 0,
            ] );
        }

        return response()->json( [
            'success'      => true,
            'plugin_exist' => false,
        ] );

    }

    private function getFullMonth( $month_index = 0 ) {
        $months = array(
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        );

        return $months[ $month_index ];
    }
}
