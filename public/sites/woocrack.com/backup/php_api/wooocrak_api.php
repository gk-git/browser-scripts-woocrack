<?php
/**
 * Created by PhpStorm.
 * User: gabykaram
 * Date: 12/8/18
 * Time: 9:26 PM
 */

require_once 'DB.php';


function downloadFile( $url, $withName ) {

	$curlCh = curl_init();
	curl_setopt( $curlCh, CURLOPT_URL, $url );
	curl_setopt( $curlCh, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $curlCh, CURLOPT_SSLVERSION, 3 );
	$curlData = curl_exec( $curlCh );
	curl_close( $curlCh );


	$downloadPath = 'downloads/' . $withName;
	$file         = fopen( $downloadPath, 'wb+' );
	fwrite( $file, $curlData );
	fclose( $file );
}

$db = new DB();

$json_return = array(
	'status'  => 'success',
	'message' => '',
	'error'   => array(),
);
if ( isset( $_GET['function'] ) ) {
	$function_name = $_GET['function'];
	if ( $function_name === 'save_urls' ) {
		$plugins_array = json_decode( $_POST['plugin_urls'] );
		foreach ( $plugins_array as $plugin_url ) {
			$db->create_plugin( $plugin_url->URL );
		}
		echo json_encode( $json_return );
	}
	if ( $function_name === 'save_downloads_url' ) {
		$plugin_download_data = array(
			'plugin_url'          => $_POST['plugin_url'],
			'plugin_download_url' => $_POST['plugin_download_url'],
			'plugin_last_update'  => $_POST['plugin_last_update'],
		);
		$db->create_plugin_download( $_POST['plugin_url'], $_POST['plugin_download_url'],
			$_POST['plugin_last_update'] );
		echo json_encode( $plugin_download_data );
	}
	if ( $function_name === 'url_exist' ) {
		$url    = $_GET['url'];
		$status = $db->find_plugin_download_by_url_id( $url );
		if ( $status ) {
			echo 'true';
		}

		return 'false';
	}
	if ( $function_name === 'download' ) {
		$start   = (int) $_GET['start'];
		$end     = (int) $_GET['end'];
		$plugins = $db->get_all_plugins_downloads();
		foreach ( $plugins as $key => $plugin ) {
			if ( $key >= $start && $end >= $key ) {
				$download = downloadFile( $plugin['plugin_download_url'],
					$filename = basename( $plugin['plugin_download_url'] ) );
//				sleep( 2 );
				echo $start . $end;
			} elseif ( $key > $end ) {
				if ( ($key+1) % 15 ===0) {
					echo 3;
				}
				break;

			}
		}
		echo json_encode( $plugins );
	}
}
