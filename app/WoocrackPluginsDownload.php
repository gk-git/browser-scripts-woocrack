<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class WoocrackPluginsDownload
 *
 * @package App
 * @property string $download_url
 * @property string $version
 * @property string $last_update
 * @property string $plugin_url
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property int $plugin_url_id
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPluginsDownload newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPluginsDownload newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\WoocrackPluginsDownload onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPluginsDownload query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPluginsDownload whereCreatedAt( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPluginsDownload whereDeletedAt( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPluginsDownload whereDownloadUrl( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPluginsDownload whereId( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPluginsDownload whereLastUpdate( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPluginsDownload wherePluginUrlId( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPluginsDownload whereUpdatedAt( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPluginsDownload whereVersion( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\WoocrackPluginsDownload withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\WoocrackPluginsDownload withoutTrashed()
 * @mixin \Eloquent
 * @property int $downloaded
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPluginsDownload whereDownloaded($value)
 */
class WoocrackPluginsDownload extends Model {
    use SoftDeletes;

    protected $fillable = [ 'download_url', 'version', 'downloaded', 'last_update', 'plugin_url_id' ];
    protected $hidden = [];


    public static function boot() {
        parent::boot();

        WoocrackPluginsDownload::observe( new \App\Observers\UserActionsObserver );
    }

    /**
     * Set attribute to date format
     *
     * @param $input
     */
    public function setLastUpdateAttribute( $input ) {
        if ( $input != null && $input != '' ) {
            $this->attributes['last_update'] = Carbon::createFromFormat( config( 'app.date_format' ) . ' H:i:s',
                $input )->format( 'Y-m-d H:i:s' );
        } else {
            $this->attributes['last_update'] = null;
        }
    }

    /**
     * Get attribute from date format
     *
     * @param $input
     *
     * @return string
     */
    public function getLastUpdateAttribute( $input ) {
        $zeroDate = str_replace( [ 'Y', 'm', 'd' ], [ '0000', '00', '00' ], config( 'app.date_format' ) . ' H:i:s' );

        if ( $input != $zeroDate && $input != null ) {
            return Carbon::createFromFormat( 'Y-m-d H:i:s', $input )->format( config( 'app.date_format' ) . ' H:i:s' );
        } else {
            return '';
        }
    }

    /**
     * Set to null if empty
     *
     * @param $input
     */
    public function setPluginUrlIdAttribute( $input ) {
        $this->attributes['plugin_url_id'] = $input ? $input : null;
    }

    public function plugin_url() {
        return $this->belongsTo( WoocrackPlugin::class, 'plugin_url_id' )->withTrashed();
    }

}
