<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Create1546186914WoocrackPluginsDownloadsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if ( ! Schema::hasTable( 'woocrack_plugins_downloads' ) ) {
            Schema::create( 'woocrack_plugins_downloads', function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->string( 'download_url' )->unique();
                $table->string( 'download_file_path' )->unique();
                $table->string( 'version' );
                $table->boolean( 'downloaded' )->default( false );
                $table->datetime( 'last_update' )->nullable();

                $table->timestamps();
                $table->softDeletes();

                $table->index( [ 'deleted_at' ] );
            } );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists( 'woocrack_plugins_downloads' );
    }
}
