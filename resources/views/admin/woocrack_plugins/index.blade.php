@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.woocrack-plugins.title')</h3>
    @can('woocrack_plugin_create')
        <p>
            <a href="{{ route('admin.woocrack_plugins.create') }}"
               class="btn btn-success">@lang('quickadmin.qa_add_new')</a>

        </p>
    @endcan

    @can('woocrack_plugin_delete')
        <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.woocrack_plugins.index') }}"
                   style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a>
            </li>
            |
            <li><a href="{{ route('admin.woocrack_plugins.index') }}?show_deleted=1"
                   style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a>
            </li>
        </ul>
        </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table
                class="table table-bordered table-striped ajaxTable @can('woocrack_plugin_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                <tr>
                    @can('woocrack_plugin_delete')
                        @if ( request('show_deleted') != 1 )
                            <th style="text-align:center;"><input type="checkbox" id="select-all"/></th>@endif
                    @endcan

                    <th>@lang('quickadmin.woocrack-plugins.fields.url')</th>
                    <th>@lang('quickadmin.woocrack-plugins.fields.author_url')</th>
                    @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                    @else
                        <th>&nbsp;</th>
                    @endif
                </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        @can('woocrack_plugin_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.woocrack_plugins.mass_destroy') }}';
        @endif
        @endcan
        $(document).ready(function() {
            window.dtDefaultOptions.ajax = '{!! route('admin.woocrack_plugins.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [
                    @can('woocrack_plugin_delete')
                    @if ( request('show_deleted') != 1 )
                {
                    data: 'massDelete', name: 'id', searchable: false, sortable: false,
                },
                    @endif
                    @endcan{
                    data: 'url', name: 'url',
                }, {data: 'author_url', name: 'author_url'},

                {data: 'actions', name: 'actions', searchable: false, sortable: false},
            ];
            processAjaxTables();
        });
    </script>
@endsection
