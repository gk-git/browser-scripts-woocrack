<?php

class DB {

	public $db;

	public function __construct() {
		try {
			$db       = new PDO( 'sqlite:woocrack.sqlite3' );
			$this->db = $db;
			$this->create_databases();
		} catch ( PDOException $exception ) {
			echo $exception->getMessage();
		}
	}

	public function destroy_db_connection() {
		$this->db = null;
	}

	public function create_databases() {

		$databases_query = [
			'CREATE TABLE IF NOT EXISTS woocrack_plugins ( 
				plugin_id INTEGER PRIMARY KEY NOT NULL,
				 plugin_url TEXT UNIQUE NOT NULL
				 )',
			'CREATE TABLE IF NOT EXISTS woocrack_plugins_downloads (
				 plugin_download_id INTEGER PRIMARY KEY,
				 plugin_download_url TEXT UNIQUE,
				 plugin_last_update TEXT NOT NULL,
				 plugin_download_plugin_id INTEGER NOT NULL
				 )',

		];

		foreach ( $databases_query as $query ) {
			$this->db->exec( $query );

		}
	}

	public function create_plugin( $plugin_url ) {

		$plugin_already_exist = count( $this->find_plugin_by_url( $plugin_url ) ) > 0;
		if ( ! $plugin_already_exist ) {
			$query     = 'INSERT INTO woocrack_plugins ( plugin_url ) VALUES ( :plugin_url )';
			$statement = $this->db->prepare( $query );

			// Bind parameters
			$statement->bindParam( ':plugin_url', $plugin_url );
			// Execute Statement
			$statement->execute();

			// Not handling error from the above for simplicity
			return true;
		}

		return false;

	}

	public function create_plugin_download( $plugin_url, $plugin_download_url, $plugin_last_updated ) {

		$plugin_url_already_exist = count( $this->find_plugin_by_url( $plugin_url ) ) > 0;
		if ( $plugin_url_already_exist ) {
			$plugin                            = $this->find_plugin_by_url( $plugin_url );
			$plugin_download_url_already_exist = count( $this->find_plugin_download_by_url( $plugin_download_url ) ) > 0;

			if ( ! $plugin_download_url_already_exist ) {
				$query     = 'INSERT INTO woocrack_plugins_downloads ( plugin_download_url, plugin_last_update, plugin_download_plugin_id ) VALUES ( :plugin_download_url, :plugin_last_update, :plugin_download_plugin_id )';
				$statement = $this->db->prepare( $query );

				$plugin_id = $plugin[0]['plugin_id'];
				// Bind parameters
				$statement->bindParam( ':plugin_download_url', $plugin_download_url );
				$statement->bindParam( ':plugin_last_update', $plugin_last_updated );
				$statement->bindParam( ':plugin_download_plugin_id', $plugin_id );
				// Execute Statement
				$statement->execute();

				// Not handling error from the above for simplicity
				return true;
			}
		}

		return false;

	}

	public function update_plugin( $plugin_url, $new_plugin_url ) {
		$plugins              = $this->find_plugin_by_url( $plugin_url );
		$plugin_already_exist = count( $plugins ) > 0;
		if ( $plugin_already_exist ) {
			$query     = "UPDATE woocrack_plugins SET plugin_url=:plugin_new_url WHERE plugin_url=:plugin_url";
			$statement = $this->db->prepare( $query );
			$statement->bindParam( ':plugin_new_url', $new_plugin_url );
			$statement->bindParam( ':plugin_url', $plugin_url );

			$statement->execute();

			return true;
		}

		return false;

	}

	public function delete_plugin( $plugin_url ) {
		$plugin_already_exist = count( $this->find_plugin_by_url( $plugin_url ) ) > 0;
		if ( $plugin_already_exist ) {
			$query     = 'DELETE FROM woocrack_plugins WHERE plugin_url=:plugin_url';
			$statement = $this->db->prepare( $query );
			$statement->bindParam( ':plugin_url', $plugin_url );
			$statement->execute();

			return true;
		}

		return false;
	}

	public function find_plugin_by_url( $plugin_url ) {
		$query     = "SELECT * FROM woocrack_plugins WHERE plugin_url = '$plugin_url'";
		$statement = $this->db->prepare( $query );
		$statement->execute();

		$results = $statement->fetchAll();

		return $results;
	}

	public function find_plugin_download_by_url( $plugin_download_url ) {
		$query     = "SELECT * FROM woocrack_plugins_downloads WHERE plugin_download_url = '$plugin_download_url'";
		$statement = $this->db->prepare( $query );
		$statement->execute();

		$results = $statement->fetchAll();

		return $results;
	}

	public function find_plugin_download_by_url_id( $plugin_url ) {

		$plugin = $this->find_plugin_by_url( $plugin_url );
		if ( count( $plugin ) > 0 ) {
			$plugin_id = $plugin[0]['plugin_id'];
			$query     = "SELECT * FROM woocrack_plugins_downloads WHERE plugin_download_plugin_id = '$plugin_id'";
			$statement = $this->db->prepare( $query );
			$statement->execute();

			$results = $statement->fetchAll();

			if(count($results)> 0){
				return true;
			}
			return false;
		}
		return false;
	}

	public function get_all_plugins() {
		$query     = 'SELECT * FROM woocrack_plugins';
		$statement = $this->db->query( $query );

		return $statement->fetchAll();
	}

	public function get_all_plugins_downloads() {
		$query     = 'SELECT * FROM woocrack_plugins_downloads';
		$statement = $this->db->query( $query );

		return $statement->fetchAll();
	}
}
