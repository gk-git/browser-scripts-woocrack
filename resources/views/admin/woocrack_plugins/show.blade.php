@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.woocrack-plugins.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.woocrack-plugins.fields.url')</th>
                            <td field-key='url'>{{ $woocrack_plugin->url }}</td>
                            <th>@lang('quickadmin.woocrack-plugins.fields.author_url')</th>
                            <td field-key='url'>{{ $woocrack_plugin->author_url }}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">

                <li role="presentation" class="active"><a href="#woocrack_plugins_downloads"
                                                          aria-controls="woocrack_plugins_downloads" role="tab"
                                                          data-toggle="tab">Woocrack plugins downloads</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">

                <div role="tabpanel" class="tab-pane active" id="woocrack_plugins_downloads">
                    <table
                        class="table table-bordered table-striped {{ count($woocrack_plugins_downloads) > 0 ? 'datatable' : '' }}">
                        <thead>
                        <tr>
                            <th>@lang('quickadmin.woocrack-plugins-downloads.fields.download-url')</th>
                            <th>@lang('quickadmin.woocrack-plugins-downloads.fields.version')</th>
                            <th>@lang('quickadmin.woocrack-plugins-downloads.fields.last-update')</th>
                            @if( request('show_deleted') == 1 )
                                <th>&nbsp;</th>
                            @else
                                <th>&nbsp;</th>
                            @endif
                        </tr>
                        </thead>

                        <tbody>
                        @if (count($woocrack_plugins_downloads) > 0)
                            @foreach ($woocrack_plugins_downloads as $woocrack_plugins_download)
                                <tr data-entry-id="{{ $woocrack_plugins_download->id }}">
                                    <td field-key='download_url'><a
                                            href="{{ $woocrack_plugins_download->download_url }}">{{ $woocrack_plugins_download->download_url }}</a>
                                    </td>
                                    <td field-key='version'>{{ $woocrack_plugins_download->version }}</td>
                                    <td field-key='last_update'>{{ $woocrack_plugins_download->last_update }}</td>
                                    @if( request('show_deleted') == 1 )
                                        <td>
                                            @can('woocrack_plugins_download_delete')
                                                {!! Form::open(array(
                'style' => 'display: inline-block;',
                'method' => 'POST',
                'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                'route' => ['admin.woocrack_plugins_downloads.restore', $woocrack_plugins_download->id])) !!}
                                                {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                                {!! Form::close() !!}
                                            @endcan
                                            @can('woocrack_plugins_download_delete')
                                                {!! Form::open(array(
                'style' => 'display: inline-block;',
                'method' => 'DELETE',
                'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                'route' => ['admin.woocrack_plugins_downloads.perma_del', $woocrack_plugins_download->id])) !!}
                                                {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    @else
                                        <td>
                                            @can('woocrack_plugins_download_view')
                                                <a href="{{ route('admin.woocrack_plugins_downloads.show',[$woocrack_plugins_download->id]) }}"
                                                   class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                            @endcan
                                            @can('woocrack_plugins_download_edit')
                                                <a href="{{ route('admin.woocrack_plugins_downloads.edit',[$woocrack_plugins_download->id]) }}"
                                                   class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                            @endcan
                                            @can('woocrack_plugins_download_delete')
                                                {!! Form::open(array(
                                                                                        'style' => 'display: inline-block;',
                                                                                        'method' => 'DELETE',
                                                                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                                                                        'route' => ['admin.woocrack_plugins_downloads.destroy', $woocrack_plugins_download->id])) !!}
                                                {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9">@lang('quickadmin.qa_no_entries_in_table')</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.woocrack_plugins.index') }}"
               class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop


