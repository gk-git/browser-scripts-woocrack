import readingTime from "reading-time";
import {enableSelection__woocrack, handleSalesPageClick} from "./utils/documents";
import {addManagementPopup} from "./utils/popup";

window.calcRT = ev => {
    var stats = readingTime(ev.value).text;
    document.getElementById("readingTime").innerText = stats;
};

/**
 * Enable document Selection disabled by Woocrack.com
 * */
enableSelection__woocrack();


addManagementPopup();

handleSalesPageClick();
