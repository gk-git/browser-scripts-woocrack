<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class WoocrackPlugin
 *
 * @package App
 * @property string $url
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\WoocrackPluginsDownload[] $woocrack_plugins_downloads
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPlugin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPlugin newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\WoocrackPlugin onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPlugin query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPlugin whereCreatedAt( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPlugin whereDeletedAt( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPlugin whereId( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPlugin whereUpdatedAt( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WoocrackPlugin whereUrl( $value )
 * @method static \Illuminate\Database\Query\Builder|\App\WoocrackPlugin withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\WoocrackPlugin withoutTrashed()
 * @mixin \Eloquent
 */
class WoocrackPlugin extends Model {
    use SoftDeletes;

    protected $fillable = [ 'url', 'author_url' ];
    protected $hidden = [];


    public static function boot() {
        parent::boot();

        WoocrackPlugin::observe( new \App\Observers\UserActionsObserver );
    }

    public function woocrack_plugins_downloads() {
        return $this->hasMany( WoocrackPluginsDownload::class, 'plugin_url_id' );
    }
}
