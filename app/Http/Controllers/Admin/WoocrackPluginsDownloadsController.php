<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreWoocrackPluginsDownloadsRequest;
use App\Http\Requests\Admin\UpdateWoocrackPluginsDownloadsRequest;
use App\WoocrackPluginsDownload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Yajra\DataTables\DataTables;

class WoocrackPluginsDownloadsController extends Controller {
    /**
     * Display a listing of WoocrackPluginsDownload.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if ( ! Gate::allows( 'woocrack_plugins_download_access' ) ) {
            return abort( 401 );
        }


        if ( request()->ajax() ) {
            $query = WoocrackPluginsDownload::query();
            $query->with( "plugin_url" );
            $template = 'actionsTemplate';
            if ( request( 'show_deleted' ) == 1 ) {

                if ( ! Gate::allows( 'woocrack_plugins_download_delete' ) ) {
                    return abort( 401 );
                }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select( [
                'woocrack_plugins_downloads.id',
                'woocrack_plugins_downloads.download_url',
                'woocrack_plugins_downloads.downloaded',
                'woocrack_plugins_downloads.version',
                'woocrack_plugins_downloads.last_update',
                'woocrack_plugins_downloads.plugin_url_id',
            ] );
            $table = Datatables::of( $query );

            $table->setRowAttr( [
                'data-entry-id' => '{{$id}}',
            ] );
            $table->addColumn( 'massDelete', '&nbsp;' );
            $table->addColumn( 'actions', '&nbsp;' );
            $table->editColumn( 'actions', function ( $row ) use ( $template ) {
                $gateKey  = 'woocrack_plugins_download_';
                $routeKey = 'admin.woocrack_plugins_downloads';

                return view( $template, compact( 'row', 'gateKey', 'routeKey' ) );
            } );
            $table->editColumn( 'plugin_url.url', function ( $row ) {
                return $row->plugin_url ? '<a href="' . $row->plugin_url->url . '" target="_blank">' . $row->plugin_url->url . '</a>' : '';
            } );
            $table->editColumn( 'download_url', function ( $row ) {
                return $row->download_url ? '<a href="' . $row->download_url . '" target="_blank">' . $row->download_url . '</a>' : '';
            } );
            $table->editColumn( 'downloaded', function ( $row ) {
                return $row->downloaded === 'false' ? '<button class="download-button" data-id="' . $row->id . '">Not Downloaded</button>' : '<button>Downloaded</button>';
            } );

            $table->rawColumns( [ 'actions', 'massDelete' ] );

            return $table->escapeColumns( [ 'version', 'last_update', 'plugin_url_id' ] )->make( true );
        }

        return view( 'admin.woocrack_plugins_downloads.index' );
    }

    /**
     * Show the form for creating new WoocrackPluginsDownload.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        if ( ! Gate::allows( 'woocrack_plugins_download_create' ) ) {
            return abort( 401 );
        }

        $plugin_urls = \App\WoocrackPlugin::get()->pluck( 'url',
            'id' )->prepend( trans( 'quickadmin.qa_please_select' ), '' );

        return view( 'admin.woocrack_plugins_downloads.create', compact( 'plugin_urls' ) );
    }

    /**
     * Store a newly created WoocrackPluginsDownload in storage.
     *
     * @param  \App\Http\Requests\StoreWoocrackPluginsDownloadsRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( StoreWoocrackPluginsDownloadsRequest $request ) {
        if ( ! Gate::allows( 'woocrack_plugins_download_create' ) ) {
            return abort( 401 );
        }
        $woocrack_plugins_download = WoocrackPluginsDownload::create( $request->all() );


        return redirect()->route( 'admin.woocrack_plugins_downloads.index' );
    }


    /**
     * Show the form for editing WoocrackPluginsDownload.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( $id ) {
        if ( ! Gate::allows( 'woocrack_plugins_download_edit' ) ) {
            return abort( 401 );
        }

        $plugin_urls = \App\WoocrackPlugin::get()->pluck( 'url',
            'id' )->prepend( trans( 'quickadmin.qa_please_select' ), '' );

        $woocrack_plugins_download = WoocrackPluginsDownload::findOrFail( $id );

        return view( 'admin.woocrack_plugins_downloads.edit', compact( 'woocrack_plugins_download', 'plugin_urls' ) );
    }

    /**
     * Update WoocrackPluginsDownload in storage.
     *
     * @param  \App\Http\Requests\UpdateWoocrackPluginsDownloadsRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update( UpdateWoocrackPluginsDownloadsRequest $request, $id ) {
        if ( ! Gate::allows( 'woocrack_plugins_download_edit' ) ) {
            return abort( 401 );
        }
        $woocrack_plugins_download = WoocrackPluginsDownload::findOrFail( $id );
        $woocrack_plugins_download->update( $request->all() );


        return redirect()->route( 'admin.woocrack_plugins_downloads.index' );
    }


    /**
     * Display WoocrackPluginsDownload.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show( $id ) {
        if ( ! Gate::allows( 'woocrack_plugins_download_view' ) ) {
            return abort( 401 );
        }
        $woocrack_plugins_download = WoocrackPluginsDownload::findOrFail( $id );

        return view( 'admin.woocrack_plugins_downloads.show', compact( 'woocrack_plugins_download' ) );
    }


    /**
     * Remove WoocrackPluginsDownload from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {
        if ( ! Gate::allows( 'woocrack_plugins_download_delete' ) ) {
            return abort( 401 );
        }
        $woocrack_plugins_download = WoocrackPluginsDownload::findOrFail( $id );
        $woocrack_plugins_download->delete();

        return redirect()->route( 'admin.woocrack_plugins_downloads.index' );
    }

    /**
     * Delete all selected WoocrackPluginsDownload at once.
     *
     * @param Request $request
     */
    public function massDestroy( Request $request ) {
        if ( ! Gate::allows( 'woocrack_plugins_download_delete' ) ) {
            return abort( 401 );
        }
        if ( $request->input( 'ids' ) ) {
            $entries = WoocrackPluginsDownload::whereIn( 'id', $request->input( 'ids' ) )->get();

            foreach ( $entries as $entry ) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore WoocrackPluginsDownload from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function restore( $id ) {
        if ( ! Gate::allows( 'woocrack_plugins_download_delete' ) ) {
            return abort( 401 );
        }
        $woocrack_plugins_download = WoocrackPluginsDownload::onlyTrashed()->findOrFail( $id );
        $woocrack_plugins_download->restore();

        return redirect()->route( 'admin.woocrack_plugins_downloads.index' );
    }

    /**
     * Permanently delete WoocrackPluginsDownload from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function perma_del( $id ) {
        if ( ! Gate::allows( 'woocrack_plugins_download_delete' ) ) {
            return abort( 401 );
        }
        $woocrack_plugins_download = WoocrackPluginsDownload::onlyTrashed()->findOrFail( $id );
        $woocrack_plugins_download->forceDelete();

        return redirect()->route( 'admin.woocrack_plugins_downloads.index' );
    }
}
