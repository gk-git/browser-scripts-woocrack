<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreWoocrackPluginsRequest;
use App\Http\Requests\Admin\UpdateWoocrackPluginsRequest;
use App\WoocrackPlugin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Yajra\DataTables\DataTables;

class WoocrackPluginsController extends Controller {
    /**
     * Display a listing of WoocrackPlugin.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if ( ! Gate::allows( 'woocrack_plugin_access' ) ) {
            return abort( 401 );
        }


        if ( request()->ajax() ) {
            $query    = WoocrackPlugin::query();
            $template = 'actionsTemplate';
            if ( request( 'show_deleted' ) == 1 ) {

                if ( ! Gate::allows( 'woocrack_plugin_delete' ) ) {
                    return abort( 401 );
                }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select( [
                'woocrack_plugins.id',
                'woocrack_plugins.url',
                'woocrack_plugins.author_url',
            ] );
            $table = Datatables::of( $query );

            $table->setRowAttr( [
                'data-entry-id' => '{{$id}}',
            ] );
            $table->addColumn( 'massDelete', '&nbsp;' );
            $table->addColumn( 'actions', '&nbsp;' );
            $table->editColumn( 'actions', function ( $row ) use ( $template ) {
                $gateKey  = 'woocrack_plugin_';
                $routeKey = 'admin.woocrack_plugins';

                return view( $template, compact( 'row', 'gateKey', 'routeKey' ) );
            } );
            $table->editColumn( 'url', function ( $row ) {
                return $row->url ? $row->url : '';
            } );
            $table->editColumn( 'author_url', function ( $row ) {
                return $row->author_url ? '<a href="' . $row->author_url . '" target="_blank">' . $row->author_url . '</a>' : '';
            } );

            $table->rawColumns( [ 'actions', 'massDelete' ] );

            return $table->escapeColumns( [] )->make( true );
        }

        return view( 'admin.woocrack_plugins.index' );
    }

    /**
     * Show the form for creating new WoocrackPlugin.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        if ( ! Gate::allows( 'woocrack_plugin_create' ) ) {
            return abort( 401 );
        }

        return view( 'admin.woocrack_plugins.create' );
    }

    /**
     * Store a newly created WoocrackPlugin in storage.
     *
     * @param  \App\Http\Requests\StoreWoocrackPluginsRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( StoreWoocrackPluginsRequest $request ) {
        if ( ! Gate::allows( 'woocrack_plugin_create' ) ) {
            return abort( 401 );
        }
        $woocrack_plugin = WoocrackPlugin::create( $request->all() );

        foreach ( $request->input( 'woocrack_plugins_downloads', [] ) as $data ) {
            $woocrack_plugin->woocrack_plugins_downloads()->create( $data );
        }


        return redirect()->route( 'admin.woocrack_plugins.index' );
    }


    /**
     * Show the form for editing WoocrackPlugin.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( $id ) {
        if ( ! Gate::allows( 'woocrack_plugin_edit' ) ) {
            return abort( 401 );
        }
        $woocrack_plugin = WoocrackPlugin::findOrFail( $id );

        return view( 'admin.woocrack_plugins.edit', compact( 'woocrack_plugin' ) );
    }

    /**
     * Update WoocrackPlugin in storage.
     *
     * @param  \App\Http\Requests\UpdateWoocrackPluginsRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update( UpdateWoocrackPluginsRequest $request, $id ) {
        if ( ! Gate::allows( 'woocrack_plugin_edit' ) ) {
            return abort( 401 );
        }
        $woocrack_plugin = WoocrackPlugin::findOrFail( $id );
        $woocrack_plugin->update( $request->all() );

        $woocrackPluginsDownloads           = $woocrack_plugin->woocrack_plugins_downloads;
        $currentWoocrackPluginsDownloadData = [];
        foreach ( $request->input( 'woocrack_plugins_downloads', [] ) as $index => $data ) {
            if ( is_integer( $index ) ) {
                $woocrack_plugin->woocrack_plugins_downloads()->create( $data );
            } else {
                $id                                        = explode( '-', $index )[1];
                $currentWoocrackPluginsDownloadData[ $id ] = $data;
            }
        }
        foreach ( $woocrackPluginsDownloads as $item ) {
            if ( isset( $currentWoocrackPluginsDownloadData[ $item->id ] ) ) {
                $item->update( $currentWoocrackPluginsDownloadData[ $item->id ] );
            } else {
                $item->delete();
            }
        }


        return redirect()->route( 'admin.woocrack_plugins.index' );
    }


    /**
     * Display WoocrackPlugin.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show( $id ) {
        if ( ! Gate::allows( 'woocrack_plugin_view' ) ) {
            return abort( 401 );
        }
        $woocrack_plugins_downloads = \App\WoocrackPluginsDownload::where( 'plugin_url_id', $id )->get();

        $woocrack_plugin = WoocrackPlugin::findOrFail( $id );

        return view( 'admin.woocrack_plugins.show', compact( 'woocrack_plugin', 'woocrack_plugins_downloads' ) );
    }


    /**
     * Remove WoocrackPlugin from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {
        if ( ! Gate::allows( 'woocrack_plugin_delete' ) ) {
            return abort( 401 );
        }
        $woocrack_plugin = WoocrackPlugin::findOrFail( $id );
        $woocrack_plugin->delete();

        return redirect()->route( 'admin.woocrack_plugins.index' );
    }

    /**
     * Delete all selected WoocrackPlugin at once.
     *
     * @param Request $request
     */
    public function massDestroy( Request $request ) {
        if ( ! Gate::allows( 'woocrack_plugin_delete' ) ) {
            return abort( 401 );
        }
        if ( $request->input( 'ids' ) ) {
            $entries = WoocrackPlugin::whereIn( 'id', $request->input( 'ids' ) )->get();

            foreach ( $entries as $entry ) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore WoocrackPlugin from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function restore( $id ) {
        if ( ! Gate::allows( 'woocrack_plugin_delete' ) ) {
            return abort( 401 );
        }
        $woocrack_plugin = WoocrackPlugin::onlyTrashed()->findOrFail( $id );
        $woocrack_plugin->restore();

        return redirect()->route( 'admin.woocrack_plugins.index' );
    }

    /**
     * Permanently delete WoocrackPlugin from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function perma_del( $id ) {
        if ( ! Gate::allows( 'woocrack_plugin_delete' ) ) {
            return abort( 401 );
        }
        $woocrack_plugin = WoocrackPlugin::onlyTrashed()->findOrFail( $id );
        $woocrack_plugin->forceDelete();

        return redirect()->route( 'admin.woocrack_plugins.index' );
    }
}
