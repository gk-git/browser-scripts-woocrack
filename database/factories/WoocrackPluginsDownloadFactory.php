<?php

$factory->define(App\WoocrackPluginsDownload::class, function (Faker\Generator $faker) {
    return [
        "download_url" => $faker->name,
        "version" => $faker->name,
        "last_update" => $faker->date("Y-m-d H:i:s", $max = 'now'),
        "plugin_url_id" => factory('App\WoocrackPlugin')->create(),
    ];
});
