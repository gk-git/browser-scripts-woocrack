<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Create1546186786WoocrackPluginsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if ( ! Schema::hasTable( 'woocrack_plugins' ) ) {
            Schema::create( 'woocrack_plugins', function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->string( 'url' )->unique();
                $table->string( 'author_url' )->nullable();
                $table->timestamps();
                $table->softDeletes();

                $table->index( [ 'deleted_at' ] );
            } );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists( 'woocrack_plugins' );
    }
}
