window.onerror = function() {
    return true;
};
let $ = jQuery;

const copyToClipboard = str => {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
};
$('.sales_page').click(function(event) {
    copyToClipboard($(this).
        html().
        replace('<center><b>SALES PAGE :</b> ', '').
        replace('</center>', ''));
    const url = $(this).
        html().
        replace('<center><b>SALES PAGE :</b> ', '').
        replace('</center>', '');
    var win = window.open(url, '_blank');
});

function getPluginsUrl() {
    let urls_array = [];
    jQuery('ul.index').each(function() {
        let plugin_element = $(this).find('li a');
        plugin_element.each(function() {
            let plugin_url = this.href.replace(/\/?$/, '/');
            urls_array.push({URL: plugin_url});
        });
    });
    return urls_array;
}

function save_plugin_download_url() {
    const plugin_downloadURL_onlcik = $('.button-join').
        parent().
        attr('onclick');
    let plugin_downloadURL = plugin_downloadURL_onlcik.replace(
        'location.href=\'', '').replace('\'', '');
    ;
    plugin_downloadURL = plugin_downloadURL.replace('\'', '');

    let plugin_url = window.location.href;
    let plugin_author_url = $('.sales_page center').
        html().
        replace('<b>SALES PAGE :</b> ', '').
        replace('</center>', '').trim();
    let plugin_last_update = $('.subscription_benefits li:last b').html();
    let plugin_last_update_date_msec = Date.parse(plugin_last_update);
    let plugin_last_update_date = new Date(plugin_last_update_date_msec);

    const plugin_last_update_day = plugin_last_update_date.getDate();
    const plugin_last_update_month = plugin_last_update_date.getMonth();
    const plugin_last_update_year = plugin_last_update_date.getFullYear();
    console.log(plugin_last_update_date);
    let plugin_version = getPluginVersion();
    save_plugin_download_data({
        plugin_url: plugin_url,
        plugin_download_url: plugin_downloadURL,
        plugin_last_update_day: plugin_last_update_day,
        plugin_last_update_month: plugin_last_update_month,
        plugin_last_update_year: plugin_last_update_year,
        plugin_version: plugin_version,
        plugin_author_url: plugin_author_url,
    });
}

function getPluginVersion() {

    var return_version = '';
    jQuery('.subscription_benefits li').each(function() {
        var version = $(this).html();
        if (version.indexOf('Product Version : ') !== -1) {
            version = version.replace(
                'Product Version : <span class="label label-success">', '');
            return_version = version.replace('</span><b></b>', '');
        }
    });

    return return_version;
}

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}

let urls_array = [];
let downloadURL_array = [];

if (window.location.href.indexOf('woocrack.com/catalog') > 0) {
    urls_array = getPluginsUrl();
}

if (window.location.href.indexOf('download') > 0) {
    save_plugin_download_url();
}

function open_urls(start = 1, end = urls_array.length) {
    if (start >= 1 && start < end) {
        for (let index = start - 1; index < end; index++) {
            const tab_url = urls_array[index].URL;
            sleep(5000);
            window.open(tab_url, '_blank');
        }
    }

}

function open_urls_2(start = 0, end = urls_array.length, urls_array) {
    if (start >= 0 && start < end) {
        for (let index = start; index < end; index++) {

            const plugin_url = urls_array[index].URL;
            $.post('https://browserscripts.tk/api/v1/plugin_exist',
                {
                    plugin_url,
                }, function(data, status) {
                    //  tab_url
                    if (data.plugin_exist === true &&
                        data.plugin_download_exist === false) {
                        window.open(plugin_url + '?close_browser', '_blank');

                    }
                });

        }
    }
}

function save_url_array(urls_array) {
    $.post(
        'https://browserscripts.tk/api/v1/woocrack_plugins_add_sets',
        {
            plugin_urls: urls_array,
        },
        function(data, status) {
        }, 'json');
}

function save_plugin_download_data({plugin_url, plugin_download_url, plugin_last_update_day, plugin_last_update_month, plugin_last_update_year, plugin_version, plugin_author_url}) {
    $.post(
        'https://browserscripts.tk/api/v1/save_downloads_url',
        {
            plugin_url,
            plugin_download_url,
            plugin_last_update_day,
            plugin_last_update_month,
            plugin_last_update_year,
            plugin_version,
            plugin_author_url,
        },
        function(data, status) {
            if (window.location.href.indexOf('close_browser') > 0) {
                window.close();
            }
        });
}

function download_plugins(start = 0, end = 5) {
    $.get('https://browserscripts.tk/api/v1/download_plugins/' +
        start + '/' + end, function(data, status) {

    });
}

window.urls_array = urls_array;
window.save_url_array = save_url_array;
window.save_plugin_download_url = save_plugin_download_url;
window.open_urls_2 = open_urls_2;
window.plugin_start = 1690;
window.plugin_counter = 15;

// window.open_urls_3 = function() {
//     window.open_urls_2(window.plugin_start, window.plugin_end,
//         window.urls_array);
//     window.plugin_start = window.plugin_end + 1;
//     window.plugin_end = window.plugin_end + 5;
//     var header = document.getElementById(
//         'header').innerHTML = window.plugin_end;
//
// };
// window.plugins_start = 1;
// window.plugins_end = 5;
// window.download_urls = function() {
//     download_plugins(window.plugins_start, window.plugins_end);
//     window.plugins_end = window.plugins_end + 5;
//     window.plugins_start = window.plugins_end - 5;
// };
// save_url_array(urls_array);
// console.log(urls_array);
// console.log(downloadURL_array);

const brand_logo = jQuery('.brand');
brand_logo.attr('href', '#');
brand_logo.append(
    '<button id="save_plugins_page_url">Save Plugins</button>',
);
brand_logo.append(
    '<button id="save_plugins_download_url">Save Plugins Download</button>',
);
brand_logo.append(
    '<button id="save_plugins_download_counter">' + window.plugin_start +
    '</button>',
);
brand_logo.on('click', '#save_plugins_page_url', function() {
    event.preventDefault();
    urls_array = getPluginsUrl();
    save_url_array(urls_array);
});

function save_down() {
    open_urls_2(window.plugin_start, window.plugin_start +
        window.plugin_counter, window.urls_array);
    window.plugin_start += window.plugin_counter;
    $('#save_plugins_download_counter').html(window.plugin_start);
}

brand_logo.on('click', '#save_plugins_download_url', function() {
    event.preventDefault();
    save_down();
});
save_down();
let download_interval = setInterval(function() {
    if (window.plugin_start < 2050) {
        save_down();
    } else {
        clearInterval(download_interval);
    }

}, 60000);
