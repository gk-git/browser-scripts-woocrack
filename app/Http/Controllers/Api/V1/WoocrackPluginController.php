<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreWoocrackPluginsRequest;
use App\Http\Requests\API\StoreWoocrackPluginsRequest as API_StoreWoocrackPluginsRequest;
use App\Http\Resources\WoocrackPluginResource;
use App\WoocrackPlugin;
use Illuminate\Http\Request;

class WoocrackPluginController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index() {
        return WoocrackPluginResource::collection( WoocrackPlugin::all() );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return WoocrackPluginResource
     */
    public function store( StoreWoocrackPluginsRequest $request ) {
        $woocrack_plugin = WoocrackPlugin::create( [
            'url' => $request->url,
        ] );

        return new WoocrackPluginResource( $woocrack_plugin );
    }

    public function stores( API_StoreWoocrackPluginsRequest $request ) {
        $plugin_urls = $request->input( 'plugin_urls' );

        $woocrack_plugins = array();
        $count            = 0;
        $count_updated    = 0;
        foreach ( $plugin_urls as $key => $plugin_url ) {

            $plugin_url = $plugin_url['URL'];
            $count ++;
            $plugin       = WoocrackPlugin::where( 'url', '=', $plugin_url )->get()->toArray();
            $plugin_count = \count( $plugin );
            if ( $plugin_count === 0 ) {
                $woocrack_plugins[] = WoocrackPlugin::create( [
                    'url' => $plugin_url,
                ] );
                $count_updated ++;
            }
        }

        return response()->json( $woocrack_plugins );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show( WoocrackPlugin $id ) {
        //
        return new WoocrackPluginResource( $id );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param WoocrackPlugin $id
     *
     * @return WoocrackPluginResource
     */
    public function update( Request $request, WoocrackPlugin $id ) {
        //
        $id->update( $request->only( [ 'title', 'description' ] ) );

        return new WoocrackPluginResource( $id );
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param WoocrackPlugin $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( WoocrackPlugin $id ) {
        //
        try {
            $id->delete();

            return response()->json( null, 204 );

        } catch ( \Exception $e ) {
            return response()->json( array( 'error' => 'not deleted' ), 204 );

        }

    }
}

