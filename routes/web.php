<?php
Route::get('/', function () { return redirect('/admin/home'); });

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index');
    
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);
    Route::resource('user_actions', 'Admin\UserActionsController');
    Route::resource('woocrack_plugins', 'Admin\WoocrackPluginsController');
    Route::post('woocrack_plugins_mass_destroy', ['uses' => 'Admin\WoocrackPluginsController@massDestroy', 'as' => 'woocrack_plugins.mass_destroy']);
    Route::post('woocrack_plugins_restore/{id}', ['uses' => 'Admin\WoocrackPluginsController@restore', 'as' => 'woocrack_plugins.restore']);
    Route::delete('woocrack_plugins_perma_del/{id}', ['uses' => 'Admin\WoocrackPluginsController@perma_del', 'as' => 'woocrack_plugins.perma_del']);
    Route::resource('woocrack_plugins_downloads', 'Admin\WoocrackPluginsDownloadsController');
    Route::post('woocrack_plugins_downloads_mass_destroy', ['uses' => 'Admin\WoocrackPluginsDownloadsController@massDestroy', 'as' => 'woocrack_plugins_downloads.mass_destroy']);
    Route::post('woocrack_plugins_downloads_restore/{id}', ['uses' => 'Admin\WoocrackPluginsDownloadsController@restore', 'as' => 'woocrack_plugins_downloads.restore']);
    Route::delete('woocrack_plugins_downloads_perma_del/{id}', ['uses' => 'Admin\WoocrackPluginsDownloadsController@perma_del', 'as' => 'woocrack_plugins_downloads.perma_del']);



 
});
