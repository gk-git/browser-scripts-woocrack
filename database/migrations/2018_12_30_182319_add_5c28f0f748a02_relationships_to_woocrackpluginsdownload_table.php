<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Add5c28f0f748a02RelationshipsToWoocrackPluginsDownloadTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table( 'woocrack_plugins_downloads', function ( Blueprint $table ) {
            if ( ! Schema::hasColumn( 'woocrack_plugins_downloads', 'plugin_url_id' ) ) {
                $table->integer( 'plugin_url_id' )->unsigned()->default( 0 );
                $table->foreign( 'plugin_url_id',
                    '247036_5c28f0a40a962' )->references( 'id' )->on( 'woocrack_plugins' )->onDelete( 'cascade' );
            }

        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table( 'woocrack_plugins_downloads', function ( Blueprint $table ) {

        } );
    }
}
